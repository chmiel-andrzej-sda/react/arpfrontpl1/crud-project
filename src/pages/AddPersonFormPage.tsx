import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { BackButton } from '../components/BackButton';
import React from 'react';
import axios from 'axios';
import { Navigate } from 'react-router-dom';

export interface AddPersonFormPageProps {
    resource: string;
    token?: string;
}

export function AddPersonFormPage(props: AddPersonFormPageProps): JSX.Element {
    const [firstName, setFirstName] = React.useState<string>("");
    const [firstNameError, setFirstNameError] = React.useState<boolean>(false);
    const [lastName, setLastName] = React.useState<string>("");
    const [lastNameError, setLastNameError] = React.useState<boolean>(false);
    const [email, setEmail] = React.useState<string>("");
    const [emailError, setEmailError] = React.useState<boolean>(false);
    const [imageUrl, setImageUrl] = React.useState<string>("");
    const [imageUrlError, setImageUrlError] = React.useState<boolean>(false);

    if (!props.token) {
        return <Navigate replace to={`/${props.resource}`} />
    }

    function handleFirstNameChange(event: React.ChangeEvent<HTMLInputElement>): void {
        setFirstName(event.target.value);
        setFirstNameError(false);
    }

    function handleLastNameChange(event: React.ChangeEvent<HTMLInputElement>): void {
        setLastName(event.target.value);
        setLastNameError(false);
    }

    function handleEmailChange(event: React.ChangeEvent<HTMLInputElement>): void {
        setEmail(event.target.value);
        setEmailError(false);
    }

    function handleImageUrlChange(event: React.ChangeEvent<HTMLInputElement>): void {
        setImageUrl(event.target.value);
        setImageUrlError(false);
    }

    function handleSendClick(): void {
        const regex: RegExp = /[a-zA-Z0-9_.-]+@[a-zA-Z0-9_.-]+\.[a-zA-Z0-9_.-]+/;
        const regexValid: boolean = regex.test(email);
        if (!firstName) {
            setFirstNameError(true);
        }
        if (!lastName) {
            setLastNameError(true);
        }
        if (!regexValid) {
            setEmailError(true);
        }
        if (!imageUrl) {
            setImageUrlError(true);
        }
        if (firstName && lastName && regexValid && imageUrl) {
            axios.post(`https://reqres.in/api/${props.resource}`, {firstName, lastName, email, imageUrl});
        }
    }

    return <div>
        <BackButton to={`${props.resource}`}/>
        <TextField
            className='text-first-name'
            label="First name"
            variant="outlined"
            fullWidth
            onChange={handleFirstNameChange}
            value={firstName}
            error={firstNameError}
        /><br/>
        <TextField
            className='text-last-name'
            label="Last name"
            variant="outlined"
            fullWidth
            onChange={handleLastNameChange}
            value={lastName}
            error={lastNameError}
        /><br/>
        <TextField
            className='text-email'
            label="E-mail"
            variant="outlined"
            type="email"
            fullWidth
            onChange={handleEmailChange}
            value={email}
            error={emailError}
        /><br/>
        <TextField
            className='text-url'
            label="Avatar URL"
            variant="outlined"
            type="url"
            fullWidth
            onChange={handleImageUrlChange}
            value={imageUrl}
            error={imageUrlError}
        /><br/>
        <Button className="button-send" variant="contained" color="success" onClick={handleSendClick}>Add</Button>
        <BackButton to="/people"/>
    </div>;
}