export interface APISingleResponse<T> {
    data: T;
}