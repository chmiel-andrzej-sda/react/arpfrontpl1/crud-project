import '../__mocks__/MockReactRouterDom';
import { shallow, ShallowWrapper } from "enzyme";
import React from "react";
import { LoginPage } from "../../src/pages/LoginPage";

describe('LoginPage', (): void => {
    it('renders', (): void => {
        // given
        const mockTokenChange: jest.Mock = jest.fn();

        // when
        const wrapper: ShallowWrapper = shallow(<LoginPage onTokenChange={mockTokenChange}/>);

        // then
        expect(wrapper).toMatchSnapshot();
        expect(mockTokenChange).toHaveBeenCalledTimes(0);
    });

    it('handles input', (): void => {
        // given
        const mockTokenChange: jest.Mock = jest.fn();
        const wrapper: ShallowWrapper = shallow(<LoginPage onTokenChange={mockTokenChange}/>);

        // when
        wrapper.find('.input-login').simulate('change', {
            target: {
                value: 'abc'
            }
        });

        wrapper.find('.input-password').simulate('change', {
            target: {
                value: 'abc'
            }
        });

        // then
        expect(wrapper).toMatchSnapshot();
        expect(mockTokenChange).toHaveBeenCalledTimes(0);
    });
});