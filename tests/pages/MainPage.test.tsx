import "../__mocks__/MockReactRouterDom";
import { shallow, ShallowWrapper } from "enzyme";
import React from "react";
import { MainPage } from '../../src/pages/MainPage'
import { mockNavigate } from "../__mocks__/MockReactRouterDom";

describe('MainPage', (): void => {
    it('renders without token', (): void => {
        // when
        const wrapper: ShallowWrapper = shallow(<MainPage/>);

        // then
        expect(wrapper).toMatchSnapshot();
    });

    it('renders with token', (): void => {
        // when
        const wrapper: ShallowWrapper = shallow(<MainPage token="abc"/>);

        // then
        expect(wrapper).toMatchSnapshot();
    });

    it('renders opened modal', (): void => {
        // given
        const wrapper: ShallowWrapper = shallow(<MainPage token="abc"/>);

        // when
        wrapper.find(".delete-users").simulate('click');

        // then
        expect(wrapper).toMatchSnapshot();
    });

    it('renders closed modal', (): void => {
        // given
        const wrapper: ShallowWrapper = shallow(<MainPage token="abc"/>);
        wrapper.find(".delete-users").simulate('click');

        // when
        wrapper.find(".dialog").simulate('close')

        // then
        expect(wrapper).toMatchSnapshot();
    });

    it('renders accepted modal', (): void => {
        // given
        const wrapper: ShallowWrapper = shallow(<MainPage token="abc"/>);
        wrapper.find(".delete-users").simulate('click');

        // when
        wrapper.find(".accept").simulate('click')

        // then
        expect(wrapper).toMatchSnapshot();
    });

    it('navigates', (): void => {
        // given
        const wrapper: ShallowWrapper = shallow(<MainPage token="abc"/>);

        // when
        wrapper.find(".navigate").simulate('click')

        // then
        expect(wrapper).toMatchSnapshot();
        expect(mockNavigate).toHaveBeenCalledTimes(1);
    });
});