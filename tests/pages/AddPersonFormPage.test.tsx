import { shallow, ShallowWrapper } from "enzyme";
import React from "react";
import { AddPersonFormPage } from '../../src/pages/AddPersonFormPage'

describe('AddPersonFormPage', (): void => {
    it('renders without token', (): void => {
        // when
        const wrapper: ShallowWrapper = shallow(<AddPersonFormPage resource="test"/>);

        // then
        expect(wrapper).toMatchSnapshot();
    });

    it('renders with token', (): void => {
        // when
        const wrapper: ShallowWrapper = shallow(<AddPersonFormPage resource="test" token="test-token"/>);

        // then
        expect(wrapper).toMatchSnapshot();
    });

    it('tries to send without data', (): void => {
        // given
        const wrapper: ShallowWrapper = shallow(<AddPersonFormPage resource="test" token="test-token"/>);

        // when
        wrapper.find('.button-send').simulate('click');

        // then
        expect(wrapper).toMatchSnapshot();
    });

    it('handles sample input data', (): void => {
        // given
        const wrapper: ShallowWrapper = shallow(<AddPersonFormPage resource="test" token="test-token"/>);

        // when
        wrapper.find('.text-first-name').simulate('change', {
            target: {
                value: 'Sample name'
            }
        });

        wrapper.find('.text-last-name').simulate('change', {
            target: {
                value: 'Sample surname'
            }
        });

        wrapper.find('.text-email').simulate('change', {
            target: {
                value: 'test@te.st'
            }
        });

        wrapper.find('.text-url').simulate('change', {
            target: {
                value: 'url'
            }
        });

        // then
        expect(wrapper).toMatchSnapshot();
    });

    it('handles sample input data after error', (): void => {
        // given
        const wrapper: ShallowWrapper = shallow(<AddPersonFormPage resource="test" token="test-token"/>);
        wrapper.find('.button-send').simulate('click');

        // when
        wrapper.find('.text-first-name').simulate('change', {
            target: {
                value: 'Sample name'
            }
        });

        wrapper.find('.text-last-name').simulate('change', {
            target: {
                value: 'Sample surname'
            }
        });

        wrapper.find('.text-email').simulate('change', {
            target: {
                value: 'test@te.st'
            }
        });

        wrapper.find('.text-url').simulate('change', {
            target: {
                value: 'url'
            }
        });

        // then
        expect(wrapper).toMatchSnapshot();
    });

    it('sends sample input data', (): void => {
        // given
        const wrapper: ShallowWrapper = shallow(<AddPersonFormPage resource="test" token="test-token"/>);
        wrapper.find('.text-first-name').simulate('change', {
            target: {
                value: 'Sample name'
            }
        });

        wrapper.find('.text-last-name').simulate('change', {
            target: {
                value: 'Sample surname'
            }
        });

        wrapper.find('.text-email').simulate('change', {
            target: {
                value: 'test@te.st'
            }
        });

        wrapper.find('.text-url').simulate('change', {
            target: {
                value: 'url'
            }
        });

        // when
        wrapper.find('.button-send').simulate('click');

        // then
        expect(wrapper).toMatchSnapshot();
    });
});