import { mockGetToken } from "./__mocks__/MockTokenStorage";
import { shallow, ShallowWrapper } from "enzyme";
import React from "react";
import { App } from '../src/App'

describe('App', (): void => {
    it('renders without token', (): void => {
        // given
        mockGetToken.mockReturnValue(undefined);

        // when
        const wrapper: ShallowWrapper = shallow(<App/>);

        // then
        expect(wrapper).toMatchSnapshot();
    });

    it('renders with token', (): void => {
        // given
        mockGetToken.mockReturnValue('test');

        // when
        const wrapper: ShallowWrapper = shallow(<App/>);

        // then
        expect(wrapper).toMatchSnapshot();
    });
});