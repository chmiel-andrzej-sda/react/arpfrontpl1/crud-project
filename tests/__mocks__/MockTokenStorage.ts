export const mockSetToken: jest.Mock = jest.fn();
export const mockGetToken: jest.Mock = jest.fn();
export const mockRemoveToken: jest.Mock = jest.fn();

jest.mock("../../src/TokenService", () => ({
    setToken: mockSetToken,
    getToken: mockGetToken,
    removeToken: mockRemoveToken
}));