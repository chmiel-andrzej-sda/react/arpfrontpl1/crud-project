import React, { PropsWithChildren } from "react";
import { NavigateFunction } from "react-router-dom";

export const mockUseParams: jest.Mock = jest.fn().mockReturnValue({});
export const mockNavigate: jest.Mock = jest.fn();

jest.mock("react-router-dom", () => ({
    useParams: mockUseParams,
    Route: (): void => undefined,
    Routes: (props: PropsWithChildren<{}>): React.ReactNode => props.children,
    useNavigate: (): NavigateFunction => mockNavigate
}));